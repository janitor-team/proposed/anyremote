#!/bin/sh

# Params
#  1. Search pattern
#  2. $(CfgDir) value

if [ "x$1" = "x" ]; then
  exit 0;
fi; 

if [ "x$2" = "x" ]; then
  exit 0;
fi; 

CANDIDATES=`find $2/Server-mode/ -type f -exec grep -l "$1" {} \;|grep -v template|grep -v svn|grep -v mediacenter`

for CFGFILE in $CANDIDATES; do
  
  BINARY=`grep "GuiAppBinary=" $CFGFILE|sed 's/GuiAppBinary=//'`
  
  LOC=`which $BINARY 2> /dev/null`
  
  if [ "x$LOC" != "x" ]; then
     APP=`echo $CFGFILE|xargs -i basename {}|sed 's/.cfg//'`
     echo $APP
  fi; 
done

#!/bin/sh

NS=`which notify-send 2> /dev/null|grep notify-send|grep -v no|wc -l|tr -d " "`

if [ "x$KD" = "x1" ]; then
	notify-send -t 2000 "$1"; 
	exit 0;
fi; 

KD=`which kdialog 2> /dev/null|grep kdialog|grep -v no|wc -l|tr -d " "`

if [ "x$KD" = "x1" ]; then
	kdialog --msgbox "$1"; 
	exit 0;
fi; 

ZN=`which zenity 2> /dev/null|grep zenity|grep -v no|wc -l|tr -d " "`

if [ "x$ZN" = "x1" ]; then
	zenity --info --text="$1"
	exit 0;
fi; 

XM=`which xmessage 2> /dev/null|grep xmessage|grep -v no|wc -l|tr -d " "`

if [ "x$XM" = "x1" ]; then
	xmessage "$1"
	exit 0;
fi; 

xterm -T anyRemote -e "echo \"$1\";read"

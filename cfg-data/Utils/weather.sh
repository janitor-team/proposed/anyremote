#!/bin/sh

# Params
#  1. -current|-forecast
#  2. location
#  3. language (ex.: en)

if [ "x$1" = "x" ] || [ "x$2" = "x" ]; then
   exit 1;
fi

LOCALIZATION=en
if [ "x$3" != "x" ]; then
   LOCALIZATION=$3
fi


PROVIDER="wttr.in/"
OPTIONS="?lang="$LOCALIZATION"'\&'"

if [ "x$1" = "x-current" ]; then
  curl -m 5 -s $PROVIDER$2${OPTIONS}0nQT
fi

if [ "x$1" = "x-forecast" ]; then
  curl -m 5 -s $PROVIDER$2${OPTIONS}2nQT
fi

if [ "x$1" = "x-temperature" ]; then
  curl -m 5 -s $PROVIDER$2${OPTIONS}0nQT|head -2|tail -1|cut -b16-
fi

exit 1;

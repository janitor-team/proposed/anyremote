//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _PR_RFCOMM_H_
#define _PR_RFCOMM_H_

#include "peer.h"

typedef struct _RfcommConnection_ {
    int       fileDescriptor;
    boolean_t useCallId;
    boolean_t hasActiveCall;
} _RfcommConnection;

int  rfcommFD     (ConnectInfo* conn);
int  rfcommSetup  (ConnectInfo* conn);
int  rfcommConnect(ConnectInfo* conn);
void rfcommClose  (ConnectInfo* conn, int final);
void rfcommReset  (ConnectInfo* conn);
int  rfcommWrite  (ConnectInfo* conn, dMessage* msg);

boolean_t rfcommCheckActiveCall(ConnectInfo* conn);
boolean_t rfcommHasActiveCall  (ConnectInfo* conn);

#endif

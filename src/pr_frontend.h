//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _PR_FRONTEND_H
#define _PR_FRONTEND_H

#include "peer.h"

// Frontend support

int  feFD    (ConnectInfo* conn);
int  feOpen  (ConnectInfo* conn);
void feClose (ConnectInfo* conn, int final);
void feReset (ConnectInfo* conn);
int  feRead  (int fd);
void feWrite (ConnectInfo* conn, const char* buf);

#endif

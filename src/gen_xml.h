//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _PR_GEN_XML_H_
#define _PR_GEN_XML_H_

#include "lib_wrapper.h"

#define XML_SOFTKEY_NUM 4

#define XML_BUTTON_PRESS  "xml_button="
#define XML_SHORT_MENU    "xml_me="
#define XML_LONG_MENU     "xml_mw="
#define XML_LIST_MENU     "xml_lm="
#define XML_LIST_MENU2    "xml_ll="
#define XML_LIST_MENU_EXT "xml_lext"
#define XML_EFIELD_CANCEL "xml_ec"
#define XML_EFIELD_SUBMIT ";?xml_ef="

string_t* renderCtrlXMLForm(string_t* ip, int port);
string_t* renderTextXMLForm(string_t* ip, int port);
string_t* renderListXMLForm(string_t* ip, int port);
string_t* renderWmanXMLForm(string_t* ip, int port);
string_t* renderEditXMLForm(string_t* ip, int port);
string_t* renderPassXMLForm(string_t* ip, int port);

string_t* sendXMLMenu(int form, string_t* ip, int port, int idx);

void      parseScreenDef (char* buffer);
void      parseCiscoModel(char* buffer);

void      renderXMLImage();

int       xmlScreenWidth();
int       xmlScreenHeight();
boolean_t xmlScreenGrayscale();
void      xmlSetLayoutOk(boolean_t ok);

#endif

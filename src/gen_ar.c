//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "executor.h"
#include "gen_html.h"
#include "mutex.h"
#include "state.h"
#include "str.h"
#include "utils.h"
#include "var.h"

extern char      tmp[MAXMAXLEN];

static void addFormMenu(int formId, string_t* page)
{
    //INFO2("addFormMenu");
    mutexLock(M_STATE);
    
    SingleList* list = userMenu();

    if (listSingleLength(list) > 0) {

        stringAppend(page, "Set(menu,replace");

        while(list) {
            if (list->data && ((string_t*) list->data)->str) {
                stringAppend(page, ",");
                stringAppend(page, ((string_t*) list->data)->str);
            }
            list = listSingleNext(list);
        }
        stringAppend(page, ");");
    }
    //INFO2("addFormMenu DONE");
    mutexUnlock(M_STATE);
}

string_t* renderCtrlForm(int port)
{
    INFO2("[WS]: renderCtrlForm");
   
    string_t* page = stringNew("Set(status,");
    
    mutexLock(M_STATE);
    stringAppend(page, cfStatus());
    stringAppend(page, ");");
    const char* font = cfFont();
    if (font) {
        stringAppend(page, "Set(font,");
        stringAppend(page, font);
        stringAppend(page, ");");
    }
    const char* bg = cfBg();
    if (bg) {
        stringAppend(page, "Set(bg,#");
        stringAppend(page, bg);
        stringAppend(page, ");");
    }
    const char* fg = cfFg();
    if (fg) {
        stringAppend(page, "Set(fg,#");
        stringAppend(page, fg);
        stringAppend(page, ");");
    }
    stringAppend(page, "Set(title,");
    stringAppend(page, cfTitle());
    stringAppend(page, ");");
    
    stringAppend(page, "Set(icons,");
    stringAppend(page, cfCaption());
    mutexUnlock(M_STATE);
    
    stringAppend(page, ",");
    
    int iconMax = (bottomlineSkin() ? ICON_NUM_BTM : ICON_NUM);
    int i = 0;
    
    mutexLock(M_STATE);
    while (i < iconMax) {
    
        if (i > 0) {
            stringAppend(page, ",");
        }

        const char* icon = cfIcon(i);
	
	    INFO2("[WS]: renderControlForm %d %s", i, (icon? icon : "NULL"));
	
        if (bottomlineSkin() && (!icon || strcmp(icon,"none") == 0)) {
             break; // skip if empty icon specified
        }

	    char maze[9];
        if (i == 9) {
            stringAppend(page,"*,");
        } else if (i == 10) {
            stringAppend(page,"0,");
        } else if (i == 11) {
            stringAppend(page,"#,");
        } else {
	        sprintf(maze,"%d,",i+1);
            stringAppend(page,maze);
        }
        
        if (!icon ) {
            stringAppend(page,"none");
	    } else {
            stringAppend(page,icon);
        }
        
        i++;
    }
    mutexUnlock(M_STATE);
    stringAppend(page, ");");
        
    if (bottomlineSkin()) {
        stringAppend(page, "Set(layout,7x1");
        if (useVolume()) {
            stringAppend(page, ",volume");
        }
        const char* up = cfUpButton();
        if (up) {
            stringAppend(page, ",up,");
            stringAppend(page, up);
        }
        const char* down = cfDownButton();
        if (down) {
            stringAppend(page, ",down,");
            stringAppend(page, down);
        }
        if (useKeypad() && !useJoystick()) {
            stringAppend(page, ",keypad_only");
        }
        if (!useKeypad() && useJoystick()) {
            stringAppend(page, ",joystick_only");
        }
        stringAppend(page, ");");
    } else {
        stringAppend(page, "Set(layout,3x4");
        if (useVolume()) {
            stringAppend(page, ",volume");
        }
        stringAppend(page, ");");
    }

    mutexLock(M_STATE);
    const char* v = cfVolume();
    if (useVolume() && v && strlen(v) > 0) {
        stringAppend(page, "Set(volume,");
        stringAppend(page, v);
        stringAppend(page, ");");
    }
    mutexUnlock(M_STATE);

    addFormMenu(CF, page);
    //printf("%s\n",page->str);
    
    return page;
}

string_t* renderCtrlFormCover()
{
    string_t* page = stringNew("Set(cover,");

    mutexLock(M_STATE);
    const char* nc = cfNamedCover();
    if (nc) {
        stringAppend(page, "by_name,");
        stringAppend(page, nc);
        stringAppend(page, ");");
    } else {
        stringAppend(page, "clear);");
    }
    mutexUnlock(M_STATE);
    
    return page;
}

string_t* renderTextForm(int port)
{
    INFO2("[WS]: renderTextForm");
    
    string_t* page = stringNew("Set(text,");
    
    mutexLock(M_STATE);
    stringAppend(page, tfCaption());
    stringAppend(page, ",replace,");
    stringAppend(page, tfText());
    mutexUnlock(M_STATE);
    
    stringAppend(page, ");");
   
    mutexLock(M_STATE);
    const char* font = tfFont();
    if (font) {
        stringAppend(page, "Set(text,font,");
        stringAppend(page, font);
        stringAppend(page, ");");
    }
    const char* bg = tfBg();
    if (bg) {
        stringAppend(page, "Set(text,bg,#");
        stringAppend(page, bg);
        stringAppend(page, ");");
    }
    const char* fg = tfFg();
    if (fg) {
        stringAppend(page, "Set(text,fg,#");
        stringAppend(page, fg);
        stringAppend(page, ");");
    }
    mutexUnlock(M_STATE);

    addFormMenu(TX, page);
    
    return page;
}

string_t* renderListForm(int port)
{
    INFO2("[WS]: renderListForm");
    
    string_t* page = stringNew("Set(list,");
    mutexLock(M_STATE);
    stringAppend(page, "replace,");
    stringAppend(page, lfCaption());
    
    SingleList* list = lfList();
    while (list) {
        stringAppend(page, ",");
        stringAppend(page, ((ListItem*) list->data)->string->str);
        
        list = listSingleNext(list);
    }
    mutexUnlock(M_STATE);
    
    stringAppend(page, ");"); 
    
    mutexLock(M_STATE);
    const char* font = lfFont();
    if (font) {
        stringAppend(page, "Set(list,font,");
        stringAppend(page, font);
        stringAppend(page, ");");
    }
    const char* bg = lfBg();
    if (bg) {
        stringAppend(page, "Set(list,bg,#");
        stringAppend(page, bg);
        stringAppend(page, ");");
    }
    const char* fg = lfFg();
    if (fg) {
        stringAppend(page, "Set(list,fg,#");
        stringAppend(page, fg);
        stringAppend(page, ");");
    }
    mutexUnlock(M_STATE);

    addFormMenu(TX, page);
    
    return page;
}

string_t* renderWmanForm(int port)
{
    INFO2("[WS]: renderWmanForm");

    string_t* page = stringNew("");

    addFormMenu(WM, page);

    return page;
}

string_t* renderEditForm(int port)
{
    INFO2("[WS]: renderEditForm askpass=%d", efPassword());

    string_t* page = stringNew("Set(editfield,");
    stringAppend(page, efCaption());
    stringAppend(page, ",");
    stringAppend(page, efLabel());
    stringAppend(page, ",");
    stringAppend(page, efText());
    stringAppend(page, ");"); 

    addFormMenu(EF, page);

    return page;
}

//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _PR_WEB_H
#define _PR_WEB_H

#include "dispatcher.h"
#include "peer.h"

enum arRequestType {
    RQ_UNKNOWN=0,
    RQ_GET,
    RQ_POST
};

typedef struct {
    int        button;
    string_t*  string;
} wMessage;

typedef struct _WebConnection_ {
    int serverFileDescriptor;
    int refreshPage;  // refrech time in seconds, -1 meand no refresh    
    int secure;
    long cookie;
    char* confDir;
    string_t* serverIP;
    SingleList *clientSockets;
} _WebConnection;

int  webFD       (ConnectInfo* connInfo);
int  openWeb     (ConnectInfo* connInfo);
void webClose    (ConnectInfo* connInfo, int final);
void webReset    (ConnectInfo* conn);
int  listenWeb   (ConnectInfo* connInfo);
int  acceptWeb   (ConnectInfo* connInfo);
int  checkWebPort();
int  writeWebConn(const dMessage* dm, int mode);
int  writeWeb    (ConnectInfo* connInfo, const dMessage* dm);
//int  haveClients (_WebConnection* cn);

void freeWMessage(void *buf);

#endif

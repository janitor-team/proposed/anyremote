//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// Single/Double linked lists
//

#ifndef _LIST_H_
#define _LIST_H_ 1

#include <lib_wrapper.h>

#ifdef USE_GLIB

typedef GSList         SingleList;

#define listSingleAppend  g_slist_append
#define listSingleLength  g_slist_length
#define listSingleNext    g_slist_next
#define listSingleNth     g_slist_nth
#define listSingleRemove  g_slist_remove
#define listSingleFree    g_slist_free

# else 

typedef struct _SingleList SingleList;

struct _SingleList
{
    void       *data;
    SingleList *next;
};

//SingleList* listSingleNew     ();
void         listSingleFree    (SingleList* list); 
void         listSingleFreeNode(SingleList* list);   // do not free data pointer
SingleList*  listSingleAppend  (SingleList* list, void* data); 
unsigned int listSingleLength  (SingleList* list);
SingleList*  listSingleNext    (SingleList* list);
SingleList*  listSingleNth     (SingleList* list, unsigned int n);
SingleList*  listSingleRemove  (SingleList* list, void* data);   // do not free data pointer

#endif

void listSingleFullFree(SingleList *list, DestroyCallback free_func);

typedef struct _DoubleList DoubleList;

struct _DoubleList
{
  void       *data;
  DoubleList *next;
  DoubleList *prev;
};

//DoubleList* listDoubleNew     ();
void        listDoubleFree    (DoubleList* list); 
void        listDoubleFreeNode(DoubleList* list);   // do not free data pointer
DoubleList* listDoublePrepend (DoubleList* list, void* data); 
DoubleList* listDoubleNext    (DoubleList* list);
void        listDoubleFullFree(DoubleList *list, DestroyCallback free_func);

#endif

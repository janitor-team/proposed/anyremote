//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

////////////////////////////////////////////////////////////////////////////////
//
// Strings
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _STR_H_
#define _STR_H_ 1

#include "lib_wrapper.h"

#ifdef USE_GLIB

#define stringNew       g_string_new	
#define stringAssign    g_string_assign
#define stringAppend    g_string_append
#define stringAppendLen g_string_append_len						      
#define stringTruncate  g_string_truncate
#define stringFree      g_string_free
#define stringHasPrefix	g_str_has_prefix   						     

#else

#include <ctype.h>

typedef struct {
    char   *str;
    size_t len;
    size_t allocated_len;
} string_t;

string_t* stringNew	 (const char *init);
string_t* stringAssign	 (string_t *string, const char *rval);
string_t* stringAppend	 (string_t *string, const char *val);
string_t* stringAppendLen(string_t *string, const char *val, size_t len);  						  
string_t* stringTruncate (string_t *string, size_t len);
char *    stringFree	 (string_t *string, boolean_t free_segment);							 
boolean_t stringHasPrefix(const char* str, const char* prefix);

#endif

#endif

//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _UTILS_H
#define _UTILS_H

#include "parse.h"

#define LOGFILE 	"/anyremote.log"

enum LogType {
    L_CFG = 0,
    L_INF,
    L_DBG,
    L_WARN,
    L_ERR
};

#define CFG2(...)     sprintf(tmp,__VA_ARGS__);logger(L_CFG,  tmp);
#define DEBUG2(...)   sprintf(tmp,__VA_ARGS__);logger(L_DBG,  tmp);
#define ERROR2(...)   sprintf(tmp,__VA_ARGS__);logger(L_ERR,  tmp);
#define INFO2(...)    sprintf(tmp,__VA_ARGS__);logger(L_INF,  tmp);
#define WARNING2(...) sprintf(tmp,__VA_ARGS__);logger(L_WARN, tmp);

#define M_ABORT 	0
#define M_DISCONNECT 	1
#define M_DEV_DISCONN 	2

#define CNV_TO 	 1
#define CNV_FROM 0

void        printHelp       (void);
void        printTime       (void);
void        printVars       (void);
void        initLog         (void);
void        releaseLog      (void);
void        logger          (int head, const char *str);

void        freeMMessage    (void *mm);

int         flushData       (void);
void        flushOldConf    (void);

int         getUidGid       (char *username, uid_t *uid, gid_t *gid);

void 	    addInternalVars (void);

void  	    sendAbort       (void);
void        sendDisconnect  (void);

int         needConvert     (void);

void        errnoDebug(const char* tag, int err);

void        stripCommandEnding(char *s);

#ifdef USE_ICONV
void        encodingHook     (void);
void        closeConvertor   (void);
char *      convCharset      (char *str, size_t size, int direction);
char *      convCharsetSimple(char *str, int direction);
#endif

#endif

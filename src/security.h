//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _SECURITY_H_
#define _SECURITY_H_

#include <lib_wrapper.h>

void      setUsePassword(boolean_t use);
boolean_t getUsePassword(void);
char* 	  getStoredPass (void);
boolean_t checkPassword (const char* value);
int       verifyPassword(int fd); // EXIT_OK/EXIT_NOK/EXIT_STOP (in case of socket error)

void      setAllowedOnly(const char* boolValue);
boolean_t isAllowed     (const char* peer);

#endif

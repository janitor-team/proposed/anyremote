//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// Message queue related wrappers
//

#ifndef _QUEUE_H_
#define _QUEUE_H_ 1

#include "list.h"

enum QueueID {
    Q_DISP = 0,
    Q_EXEC,
    Q_MAIN,
    Q_WEB,
    Q_MAX
};

int   queueNew   (int id);
void* queuePop   (int id); 
int   queueCanPop(int id);
int   queuePush  (int id, void* data);
int   queueExists(int id);
void  queueRemove(int id, DestroyCallback func);

#endif

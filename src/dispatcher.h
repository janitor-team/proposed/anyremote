//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _DISPATCHER_H_
#define _DISPATCHER_H_

#include "str.h"

enum DispMsgType {
    DM_SET = 0,
    DM_GET,
    DM_SETFILE,
    DM_SENDB,
    DM_SENDS,
    DM_CKPD,
    //DM_CMER,
    DM_EVENT,
    DM_FRONTEND,
    DM_TYPE_MAX
};


typedef struct {
    int   peer;
    int   type;
    int   subtype;
    int   size;
    void* value;
    char* file;
    char* scaled;  // scaled file name
} dMessage;

pointer_t dispatcherRoutine (pointer_t thread);
void  	  sendToDispatcher  (dMessage *buf);
void  	  freeDMessage      (void* ptr);
dMessage* allocDMessage();
void      parseCommand      (int peerId, char* cmd);
void      closePort         (int final);
char*     readFromFile      (const char *cmdTag, const char* file, int* size);
dMessage* getDFinalizer     (void);

#endif

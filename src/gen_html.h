//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _PR_GEN_HTML_H_
#define _PR_GEN_HTML_H_

#include "lib_wrapper.h"

enum arCookieStatus {
    NO_COOKIE=1,
    NEED_SEND_COOKIE,
    COOKIE_SENT
};

void      initHtmlGenerator();

string_t* renderCtrlHTMLForm(string_t* ip, int port);  
string_t* renderTextHTMLForm(string_t* ip, int port);   
string_t* renderListHTMLForm(string_t* ip, int port);
string_t* renderWmanHTMLForm(string_t* ip, int port);
string_t* renderEditHTMLForm(string_t* ip, int port);
string_t* renderPassHTMLForm(string_t* ip, int port);

int  htmlScreenWidth ();
int  htmlScreenHeight();

#endif

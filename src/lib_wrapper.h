//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// On OpenWRT size of GLIB package is more than 700kb, what is nearly unacceptable
// for embedded systems. For such systems use custom made wrappers.
//

#ifndef _LIBWRAPPER_H_
#define _LIBWRAPPER_H_ 1

#ifdef USE_GLIB

#include <glib.h>

typedef gpointer       pointer_t;
typedef gboolean       boolean_t;
typedef GString        string_t;
typedef GDestroyNotify DestroyCallback;

#define CAST_INT_TO_POINTER GINT_TO_POINTER
#define CAST_POINTER_TO_INT GPOINTER_TO_INT
#define BOOL_NO             FALSE
#define BOOL_YES            TRUE

#else

typedef void*        pointer_t;
typedef int          boolean_t;
typedef void (*DestroyCallback) (void* data);

#define CAST_INT_TO_POINTER(i) ((void*)  (long) (i))
#define CAST_POINTER_TO_INT(p) ((int)    (long) (p))

#define MAX_VALUE(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN_VALUE(a, b)  (((a) < (b)) ? (a) : (b))

#define BOOL_NO	      (0)
#define BOOL_YES      (!BOOL_NO)

#endif

#endif

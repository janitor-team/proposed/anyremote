//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _PR_STATE_H_
#define _PR_STATE_H_

#include "lib_wrapper.h"
#include "dispatcher.h"

#define ICON_NUM     12
#define ICON_NUM_BTM 7

enum arForm {
    CF=1,
    TX,
    LI,
    FM,
    WM,
    EF 
};

typedef struct {
    string_t* font;
    string_t* fg;
    string_t* bg;
} Visuals;

typedef struct {
    string_t* caption;
    string_t* title;
    string_t* status;
    string_t* icons[ICON_NUM];
    string_t* hints[ICON_NUM];
    string_t* cover;
    string_t* namedCover;
    string_t* volume;
    string_t* upBtn;
    string_t* dnBtn;
    boolean_t keypad;
    boolean_t joystick;
    boolean_t useVolume;
    boolean_t layout7x1;
    Visuals   visual;
} ControlForm;

typedef struct {
    string_t*   icon;
    string_t*   string;
} ListItem;

typedef struct {
    string_t*   caption;
    SingleList* items;
    int         selIdx;
    Visuals     visual;
} ListForm;

typedef struct {
    string_t* caption;
    string_t* text;
    Visuals   visual;
} TextForm;

typedef struct {
    string_t* window;
} WindowForm;

typedef struct {
    string_t* caption;
    string_t* text;
    string_t* label;
    boolean_t pass;
} EditForm;

typedef struct {
    ControlForm cf;
    ListForm    lf;
    TextForm    tf;
    WindowForm  wf;
    EditForm    ef;
} ClientState;

////////////////////////////////////////////////////////////////////////////////

void initState();
void freeState();
void updateState(dMessage* dm);

////////////////////////////////////////////////////////////////////////////////

void setCurForm(int f);
int  curForm   ();

void setFgBg (int form, boolean_t set_fg);	// rely on previous strtok()
void setFont (int form);	// rely on previous strtok()

////////////////////////////////////////////////////////////////////////////////

void        setupDefMenu();
SingleList* userMenu();
int         menuSize();
SingleList* menuNth (int n);
void        setMenu();     // rely on previous strtok()
void        freeMenu();    

////////////////////////////////////////////////////////////////////////////////

void setCfCaption(const char * s);
void setCfTitle  (const char * s);
void setCfStatus (const char * s);
void setIcons    ();       // rely on previous strtok()
void setHints    ();       // rely on previous strtok()
void setSkin     ();       // rely on previous strtok()
void setCfCover  (const char * s);
void setCfVolume (const char * s);

const char* cfCaption  ();
const char* cfTitle    ();
const char* cfStatus   ();
const char* cfIcon     (int i);
const char* cfHint     (int i);
const char* cfCover    ();
const char* cfNamedCover();
const char* cfBg       ();
const char* cfFg       ();
const char* cfFont     ();
void        freeCfCover();
const char* cfVolume   ();
boolean_t   useVolume  ();
boolean_t   bottomlineSkin();

boolean_t   useKeypad();
boolean_t   useJoystick();
const char* cfUpButton();
const char* cfDownButton();
string_t*   findNamedCover(const char *name);

////////////////////////////////////////////////////////////////////////////////

void setList(boolean_t useIcons);	// rely on previous strtok()

const char* lfCaption ();
SingleList* lfList    ();
SingleList* lfListNth (int n);
const char* lfBg      ();
const char* lfFg      ();
const char* lfFont    ();

void        freeLfList();
int         lfSize    ();
int         lfIndex   ();


////////////////////////////////////////////////////////////////////////////////

void setText(boolean_t split);		// rely on previous strtok()

const char* tfCaption ();
const char* tfText    ();
const char* tfBg      ();
const char* tfFg      ();
const char* tfFont    ();

////////////////////////////////////////////////////////////////////////////////

void setEditfield();	// rely on previous strtok()
void setPassField();
void setEfPassword(boolean_t use);

const char* efCaption ();
const char* efLabel   ();
const char* efText    ();
boolean_t   efPassword();

////////////////////////////////////////////////////////////////////////////////

void setImage(const char* cmd, const char* file);

const char* wfImage  ();

////////////////////////////////////////////////////////////////////////////////

void setParams();	// rely on previous strtok()
int iconPadding();
int iconSize();

#endif

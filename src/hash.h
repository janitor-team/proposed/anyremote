//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// Hash related wrappers
//

#ifndef _HASH_H_
#define _HASH_H_ 1

#include <lib_wrapper.h>

#ifdef USE_GLIB

#include <glib.h>

typedef GHFunc     HashForeachFunc;
typedef GHashTable HashTable;

# else 

#define NR_BUCKETS 256

struct StrHashNode {
    char *key;
    void *value;
    struct StrHashNode *next;
};

struct StrHashTable {

    struct StrHashNode* buckets[NR_BUCKETS];
    DestroyCallback     free_value;
};

typedef void (*HashForeachFunc) (void* key, void* value, void* data);
typedef struct StrHashTable HashTable;

#endif

HashTable* hashNew(DestroyCallback func);

void  hashReplace(HashTable* hash, const char* key, void *value); 
void* hashFind   (HashTable* hash, const char* key); 
int   hashRemove (HashTable* hash, const char* key);
void  hashDestroy(HashTable* hash);
void  hashForeach(HashTable* hash, HashForeachFunc func, void *data);

#endif

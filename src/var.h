//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

//
// $(variables) handling
//

#ifndef _VAR_H_
#define _VAR_H_ 1

typedef struct VV {
    char *value;
    int  size;
} varData;

int         setVarSimple    (const char *name, const char *val);
int         setVar          (const char *name, const char *val, int size);
varData*    searchVar       (const char *id);
void        freeVars        (void);
const char* getVarValue     (const char *name, int *sz);
char*       dupVarValue     (const char *name);

#endif

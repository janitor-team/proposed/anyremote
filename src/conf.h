//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _CONF_H_
#define _CONF_H_

#include "parse.h"
#include "list.h"
#include "mode.h"

char* getAT_CMER     	(int what);
char* getCharset        (void);
char* getDevice         (void);
char* getToMainMenu     (void);
char* getServiceName    (void);
int   autoConnect       (void);
int   getAutoRepeat     (void);
int   getBaudrate       (void);
int   getDebug          (void);
int   getFrontEnd       (void);
int   getModel          (void);
int   getRetrySecs  	(void);
int   getWaitTime       (void);
int   getUseScreen      (void);
int   getIViewer        (void);
void  freeCfg           (void);
void  freeCmds          (SingleList* commands);
void  printConf         (void);
void  printKeys         (type_key* k);
void  setModel          (char *m);
void  setInitDone       (void);

boolean_t getLog        (void);
boolean_t getBemused    (void);
boolean_t getIViewer    (void);

void  setLog            (const char* value);
void  setBemused        (const char* value);
void  setIViewer        (const char* value);
void  setWaitTime       (const char* value);

void  setRepeatNow       (type_key *k);
type_key* repeatNow     ();

type_key* findItem	(const char *key, int *flag, cmdParams *param);
type_key* findExact     (mode *mode, const char *key);
SingleList* getCommand	(type_key* item);

void  setBtAddress (char* a);
char* getBtAddress ();
void  freeBtAddress(void);

boolean_t boolValue(const char* value);

#endif

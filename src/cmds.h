//
// anyRemote
// a wi-fi or bluetooth remote for your PC.
//
// Copyright (C) 2006-2016 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//

#ifndef _CMDS_H
#define _CMDS_H

#include "parse.h"

#define PARAM_START     "$("
#define PARAM_CALLID    "CallId)"
#define PARAM_MODE      "Mode)"
#define PARAM_PARAM     "Param)"
#define PARAM_INDEX     "Index)"
#define PARAM_CFGDIR    "CfgDir)"
#define PARAM_HOME      "Home)"
#define PARAM_BTADDR    "BtAddr)"

#define SEND_STRING     "string"
#define SEND_BYTES      "bytes"

void	setToFile	(void);
int  	handleCmdByKey  (int peer, type_key* k, cmdParams *p);
int     handleCmdByKeyEx(int peer, type_key* k, cmdParams* p, int sendToMain);
int 	execDynamically (char *command);
void	flushOldConf	(void);
int	handleHook      (int hook);
char*   executeCommandPipe(const char* exec, size_t* sz);

#endif
